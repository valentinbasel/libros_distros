\selectlanguage *{spanish}
\contentsline {part}{I\hspace {1em}Introducción}{6}{part.1}%
\contentsline {chapter}{\numberline {1}Una historia sobre distros}{7}{chapter.1}%
\contentsline {chapter}{\numberline {2}¿Por qué usar Linux?}{9}{chapter.2}%
\contentsline {chapter}{\numberline {3}Sobre distros y escritorios}{12}{chapter.3}%
\contentsline {chapter}{\numberline {4}Consejos para la instalación}{16}{chapter.4}%
\contentsline {part}{II\hspace {1em}Sobre las Distros}{18}{part.2}%
\contentsline {chapter}{\numberline {5}Introducción a la segunda parte}{19}{chapter.5}%
\contentsline {chapter}{\numberline {6}Debian}{22}{chapter.6}%
\contentsline {chapter}{\numberline {7}Ubuntu}{28}{chapter.7}%
\contentsline {chapter}{\numberline {8}Fedora}{35}{chapter.8}%
\contentsline {chapter}{\numberline {9}Xubuntu}{42}{chapter.9}%
\contentsline {chapter}{\numberline {10}Manjaro}{49}{chapter.10}%
\contentsline {chapter}{\numberline {11}Mint MATE}{62}{chapter.11}%
\contentsline {chapter}{\numberline {12}Huayra}{74}{chapter.12}%
\contentsline {chapter}{\numberline {13}Trisquel}{83}{chapter.13}%
\contentsline {chapter}{\numberline {14}Preguntas Frecuentes}{91}{chapter.14}%
\contentsline {chapter}{\numberline {15}Glosario.}{97}{chapter.15}%
