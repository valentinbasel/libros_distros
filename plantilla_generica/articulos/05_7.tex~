\chapter{Huayra}
\section*{¿Qué distro usás en tu PC? ¿Por qué la preferís sobre otras?}

Cuando comenzamos con este libro algo que dijimos es que el software libre y las distribuciones de Linux en particular pueden ser tomadas por un grupo de personas puede adaptarlo a sus propias necesidades. En ese sentido no es nada loco pensar ¿Por qué no armar una versión de Linux que esté especialmente pensada para educación? Pero... ¿Qué quiere decir que una distribución está pensada para educación? ¿Está pensada para el uso de los estudiantes? ¿De los docentes? ¿Qué cosas son necesarias que estén instaladas en las computadoras de las escuelas? Huayra es precisamente una respuesta desde el estado Argentino a estas preguntas, junto con lo que era el programa conectar igualdad.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{Imagenes/huayra1.png}
    \caption{Huayra GNU/Linux}
    \label{fig:mint1}
  \end{center}
\end{figure}

Según su propia página, \href{https://huayra.conectarigualdad.gob.ar/}{https://huayra.conectarigualdad.gob.ar/} podemos decir que:

\begin{center}
“ \textbf{Huayra GNU/Linux} es un sistema operativo desarrollado por \href{http://educarse.com.ar/}{Educ.ar SE} para los \textbf{Programas Conectar Igualdad y Primaria Digital}. Siguiendo los principios y libertades del software libre, \textbf{Huayra} puede ser utilizado, estudiado, modificado y redistribuido. Está basado en \textbf{Debian}, que es una distribución reconocida y robusta” 
\end{center}

Aquí me gustaría parar para aclarar algunas cosas. Si bien en la mayoría de las escuelas argentinas no continuó el programa conectar igualdad, Huayra puede ser utilizado en cualquier computadora, de hecho durante un buen tiempo fue mi sistema operativo principal. Por lo que hoy en día si queremos utilizarlo en nuestras computadoras personales o en las computadoras de las escuelas tranquilamente podemos descargarlo e instalarlo. Por otro lado, las capturas de pantallas y explicaciones las voy a realizar con la versión 3.2 de Huayra, si bien a la hora de escribir este artículo se está desarrollando la versión 4, todavía no está disponible.

\section*{¿Qué motivos tengo para usar Huayra?}

Si lo que interesa es el uso de las computadoras en las escuelas, hay muchas razones para pensar en Huayra. La primera es que trae ya de por sí instalados \textbf{muchos programas}, no sólo aquellos que son necesarios para el día a día como escribir textos, planillas de cálculo y presentaciones (LibreOffice), navegar por internet (Firefox), ver videos y escuchar música, sino que también trae software específico para diversas áreas curriculares, simuladores de experimentos, juegos didácticos, enseñanza de la programación, software para producción audiovisual, etc. Esto nos permite tener a mano desde un principio todos esos programas para experimentar y no tener que buscarlos , descargarlos e instalarlos uno por uno para probarlos. Además si tenemos Huayra en todas las máquinas de la escuela (o curso) sabemos que esos programas están disponibles en cada una de esas máquinas y contamos con ellos para nuestras planificaciones y trabajar con nuestros estudiantes. 

	Por otro lado una gran ventaja que le veo en las escuelas es que \textbf{no hay problemas de virus}, es decir, la mayor parte de las versiones de Linux tienen casi cero problemas de virus, lo que hace que una vez que las máquinas de la escuela tengan Huayra no tengamos muchos inconvenientes por lo menos en ese sentido que es el más usual.
	
	Huayra trae como entorno de escritorio (la forma en que se muestran los menús, botones, barras de títulos, etc) Mate, un entorno liviano y parecido a la forma de uso de Windows 7, esto trae 2 ventajas, la primera es que hace que Huayra sea \textbf{liviano}, por lo que podemos instalarlo casi en cualquier computadora del colegio, inclusive esas primeras del 2010 y por otro lado que no es difícil acostumbrarse a su forma de uso ya que es una interfaz parecida a la que estamos acostumbrados.
	
	Hay más razones, pero quisiera nombrar por último que una de las problemáticas que se quiso abordar desde Huayra fue la dificultad en la conexión a internet en las escuelas por lo que Huayra \textbf{viene con software para bajar contenidos educativos en la netbook}, a su vez que compartir archivos sin necesidad de internet. 

\section*{¡Buenísimo! Tengo instalado Huayra en mi computadora... ¿Y ahora?}

Para mi el primer consejo es comenzar a explorar los programas que tenemos instalados y ver que hace cada uno. Lo segundo es tratar de reproducir en Huayra las acciones que estoy acostumbrado/a realizar en otro sistema operativo, por ejemplo negar por internet, abrir un editor de textos, escribir un poco, guardar el archivo, ver donde quedó. Mandar un email, con archivo adjunto, buscar un video, etc.

\section*{¿Entonces por donde empiezo? Escritorio y menú inicio:}

Para empezar a usar Huayra encuentran el botón inicio típico, pero en este caso arriba a la izquierda, para abrir un programa no tienen más que hacer clic en el menú inicio y de ahí ir buscar el programa que quieren abrir según la categoría  y allí hacer clic en programa que quieren abrir.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{Imagenes/huayra2.png}
    \caption{Menú de aplicaciones de Huayra}
    \label{fig:huayra2}
  \end{center}
\end{figure}

\begin{itemize}
  \item Navegar por internet – Firefox 
  \item Escuchar música – Clementina
  \item Ver videos – VLC
  \item Office – LibreOffice (procesador de textos, planilla de cálculos, presentaciones, etc)
  \item Editar imágenes – Gimp
  \item Leer emails  - Thunderbird
  \item Ver PDF – Atril
  \item Explorar archivos y carpetas – Caja
\end{itemize}

Huayra además de los programas típicos de las otras distribuciones apuntadas al uso diario trae algunos otros programas que pueden hacer más fácil la vida del docente o del estudiante.

\begin{itemize}
  \item Recordmydesktop: Grabar la pantalla y audio para hacer video tutoriales
  \item Huayra Compartir: Para compartir archivos entre computadoras conectadas a la misma red wifi
  \item Huayra Motion: para hacer películas con stop motion
  \item Huayra Caripela: Para crear avatares (representaciones de uno en forma de dibujo)
  \item Huayra Currículum: Para armar currículum
  \item CDPedia: Para hacer una copia en la computadora de la Wikipedia (y acceder a ella sin internet).
\end{itemize}

Aclaración: ¡Cuando abro un programa no me aparece en la barra de menú como siempre! ¿Qué pasó? 
Huayra tiene una barra abajo donde aparecen accesos directos para varios programas, a su vez en esa barra también aparecen los programas abiertos, aunque se ven un tanto diferentes.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{Imagenes/huayra3.png}
    \caption{Barra de menú baja}
    \label{fig:huayra2}
  \end{center}
\end{figure}

\section*{Administrar archivos}

Para encontrar nuestros archivos, así como en Windows tenemos la carpeta mis documentos , acá podemos ir a la sección “lugares” que está arriba y allí Carpeta Personal.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{Imagenes/huayra4.png}
    \caption{Lugares}
    \label{fig:huayra2}
  \end{center}
\end{figure}

Dentro de la carpeta personal encontramos varias carpetas con nombres muy sugerentes, Documentos, Imágenes, Música, etc, que nos sugieren cómo ordenar nuestra información. La carpeta descargas es donde usualmente se descargan las cosas cuando navegamos en internet. Y la carpeta Escritorio tiene todos los archivos que vemos en el escritorio. Si introducimos un pendrive o un CD nos va a figurar arriba a la izquierda donde dice Dispositivos.

\section*{Personalización de temas, iconos, fondo de pantalla, etc.}

En linux es muy fácil cambiar el aspecto de nuestro escritorio y esto me encanta (si en windows podés cambiar el fondo de pantalla, y no mucho más de manera sencilla). Para cambiar la apariencia de huayra debemos ir a “Sistema” -> Preferencias -> Apariencia, o simplemente hacer click derecho en el escritorio y allí en “cambiar fondo de escritorio”

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{Imagenes/huayra5.png}
    \caption{Menú de apariencia}
    \label{fig:huayra2}
  \end{center}
\end{figure}

En tema podemos cambiar el color de la barra de títulos, botones, texto, etc. Si queremos jugar un poco más con cada detalle podemos ir a Personalizar
En fondo, cambiamos la imagen de fondo del escritorio Y en tipografía cambiamos el tipo de letra que queremos usar.

Si queremos agregar más iconos, fondos, tipos de letra, etc. Podemos buscar en \href{https://www.mate-look.org/browse/cat/}{https://www.mate-look.org/browse/cat/} (los temas que pueden usar son los que dicen GTK2)

\section*{Usar internet}

Para navegar por internet solo tienen que ir a “Aplicaciones” → “Internet” → “Mozilla Firefox”. El navegador web que viene en Huayra es Firefox. Si bien es relativamente conocido mucha más gente conoce Chrome, el navegador de google, pero yo prefiero firefox. ¿Por qué firefox? Porque está mucho más orientado a la privacidad y seguridad. Un miedo que pueden tener es que no puedan navegar por sus páginas, pero no se preocupen que pueden usar internet como siempre. Si usan firefox en varias máquinas (inclusive en el celular) pueden crearse una cuenta para sincronizar las contraseñas, cuentas, pestañas y extensiones entre los distintos dispositivos.

Por último les dejo algunas extensiones que pueden ser útiles para firefox:

\begin{itemize}
  \item \href{https://addons.mozilla.org/es/firefox/addon/ublock-origin/}{Ublock}: Bloqueador de publicidad esto además carga mas rapido las paginas y evita que hagamos click en links “malignos”
  \item \href{https://addons.mozilla.org/es/firefox/addon/screenshot-capture-annotate/}{Awesome Screen Capture}: Captura parte de páginas como imágenes y permite editar
  \item \href{https://addons.mozilla.org/es/firefox/addon/tomato-clock/}{Tomato clock}: Permite poner contadores de tiempo para el método de organización de tiempo pomodoro. 
  \item \href{https://addons.mozilla.org/es/firefox/addon/pdf-mage/}{PDF Mague}: Crear un versión pdf de una página web
  \item \href{https://addons.mozilla.org/es/firefox/addon/wayback-machine_new/}{Wayback Machine}: Cuando una página está caida te carga la version guardada en archive.org
  \item \href{https://addons.mozilla.org/es/firefox/addon/simple-tab-groups/}{Simple Tab Groups}: Administración de grupos de pestañas (la uso para cambiar entre trabajos)
  \item \href{https://addons.mozilla.org/es/firefox/addon/savefromnet-helper/}{Savefrom helper}: Descargar videos o audios de internet
  \item \href{https://addons.mozilla.org/es/firefox/addon/privacy-badger17/}{Privacy Badger}:  Bloquea rastreadores
  \item \href{https://support.mozilla.org/es/kb/guarda-paginas-leer-despues-pocket-firefox}{Pocket}: Agendar páginas de internet y clasificarlas con etiquetas
\end{itemize}

\section*{Ofimática (Programas de office)}

Además de navegar por internet la mayoría de las personas usan programas para producir textos, hacer cálculos o armar presentaciones. Para todo esto Huayra viene con dos opciones Free Office o LibreOffice (se elige en el momento de la instalación) yo prefiero LibreOffice, principalmente por que es un producto de calidad, que viene desarrollándose con mucha continuidad y tiene una comunidad muy grande de gente trabajando alrededor de este proyecto.
Los programas para edición son:
\begin{itemize}
  \item Editar texto: LibreOffice Writer
  \item Planilla de Cálculo: Libreoffice Calc
  \item Presentaciones: LibreOffice Impress
\end{itemize}

\section*{¿Qué otras cosas puedo hacer en Huayra con los programas que ya están instalados?}

\begin{itemize}
  \item \href{https://twitter.com/HuayraLinux/status/1110599117758255106}{Organizá tu música con Clementine}
  \item \href{https://twitter.com/HuayraLinux/status/1113848710796939264}{Creá videos gif con Gimp}
  \item \href{https://twitter.com/HuayraLinux/status/1120739908870656000}{Hacé tus propios videos con Openshot}
  \item \href{https://twitter.com/HuayraLinux/status/1171136822002540544}{Animá en 2d con Tupi}
  \item \href{https://twitter.com/HuayraLinux/status/1161343449506951168}{Producí tu música con Lmms}
  \item \href{https://twitter.com/HuayraLinux/status/1153367649847533568}{Inventá fotos 3d}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Hacé\_tus\_propios\_audios\_con\_Audacity}{Hacé tus propios audios con Audacity}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Blender\_desde\_0:\_la\_interfaz}{Diseñá y animá en 3D con Blender}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Mezclá\_tu\_música\_con\_Mixxx}{Mezclá tu música con Mixxx}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Convertí\_tu\_netbook\_en\_un\_proyector\_Holográfico}{Convertí tu netbook en un proyector Holográfico}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Organizá\_tus\_PDF\_y\_ebooks\_con\_Calibre}{Organizá tus PDF y ebooks con Calibre}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Pilas}{Aprendé a programar videojuegos con pilas Engine}
  \item \href{https://huayra.educar.gob.ar/wiki-archive/Tutoriales\_Huayra\#Rob.C3.B3tica}{Programa robots con Arduino o con Ícaro}
\end{itemize}

\textbf{Más Programas}: Si quieren pueden ver una lista completa de los programas que hay en Huayra 1,2 y 3 en \href{https://cryptpad.fr/sheet/#/2/sheet/view/2Sc12Iv7NMCfjcA6VDuFF-xA5VIVmFHXHkcLVcBqYxM/}{este link} (se demora un poco en entrar la primera vez) encontrarán el nombre de cada programa, una descripción y en algunos casos tutoriales y guías. 

\section*{¿Cómo instalo más programas?}

Como casi todo sistema Linux la forma de instalar programas es a través de una especie de “play store”. En el caso de Huayra hay un programa que permite buscar y agregar más programas, lo encontramos en “Sistema” → “Administración” → “Gestor de paquetes Synaptic” , al abrirlo nos va a pedir el la contraseña, apenas instalamos Huayra el usuario y la contraseña son “alumno”, al ingresar dicha contraseña nos permite ingresar al sistema de gestión de programas. Supongamos que quiero instalar el editor de video libre Kdenlive.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{Imagenes/huayra6.png}
    \caption{Synaptic}
    \label{fig:huayra2}
  \end{center}
\end{figure}

\begin{enumerate}
  \item Abrí Synaptic
  \item Presioná el botón buscar
  \item Escribo “kdenlive” y presioná buscar (o enter con el teclado)
  \item De la lista de programas buscá kdenlive y hacele doble click
  \item Presioná “Marcar” para indicar que querés instalar todos esos archivos
  \item Hacé clic en el botón “Aplicar” para aplicar los cambios
  \item Confirmá presionando “Aplicar”
\end{enumerate}

\section*{Recomendaciones para la instalación.}
Podés descargar las ISO (archivo de instalación de huayra) en la sección \href{https://huayra.conectarigualdad.gob.ar/\#descargar}{descargas} de la página oficial. Quizás te sirva este \href{https://wiki-huayra.conectarigualdad.gob.ar/index.php/Crear\_un\_pendrive\_con\_Huayra}{tutorial de cómo generar un Pendrive} e \href{https://wiki-huayra.conectarigualdad.gob.ar/index.php/Instalación}{instalar Huayra} en una computadora.

\textbf{Nota}: Cuando la quieras descargar vas a encontrar que vienen 2 versiones. 32 bits y 64 bits. Esto depende del procesador. Las máquinas más viejas son de 32 bits por lo que hay que instalar esa versión de huayra. Las máquinas que ya venían con windows 8 o windows 10 tienen procesadores de 64 bits por lo que hay que instalar esa otra versión de huayra.

\section*{¿Dónde puedo encontrar ayuda y más información sobre Huayra?}
\begin{itemize}
  \item Twitter: \href{https://twitter.com/HuayraLinux}{https://twitter.com/HuayraLinux}
  \item Facebook: \href {https://www.facebook.com/HuayraLinux/}{https://www.facebook.com/HuayraLinux/}
  \item Grupo de Telegram: \href{https://t.me/huayra\_comunidad}{https://t.me/huayra\_comunidad}
  \item Vieja Página Oficial: \href{https://huayra.conectarigualdad.gob.ar/}{https://huayra.conectarigualdad.gob.ar/}
  \item Nueva Página Oficial (2020): \href {https://huayra.educar.gob.ar/}{https://huayra.educar.gob.ar/}
  \item Guías y tutoriales: \href{https://huayra.educar.gob.ar/wiki-archive/}{https://huayra.educar.gob.ar/wiki-archive/}
  \item Guías y tutoriales Bckp: \href{https://wiki-huayra.conectarigualdad.gob.ar}{https://wiki-huayra.conectarigualdad.gob.ar}
\end{itemize}

Otra opción es que escribas en los grupos de estudio o de telegram de clementina.org.ar donde docentes nos juntamos a intercambiar sobre software libre, cultura libre y educación.

\section*{¿Qué otras distribuciones de linux educativas puedo probar?}
\begin{itemize}
  \item \href{https://www.ubermix.org/}{Ubermix}: El Ubermix es un sistema operativo basado en Linux, totalmente gratuito y especialmente diseñado, diseñado desde cero con las necesidades de la educación en mente. Está basado en Ubuntu. Última versión 2020
  \item \href{https://academixproject.com/es/academix-gnu-linux-2/}{AcademixEdu}: Basado en Debian La distribución de Academix Linux incluye un asistente de instalación que se puede usar para instalar una variedad de aplicaciones de diversos dominios: matemática, física, química, geografía, biología, estadística, electrónica, radioaficionados, gráficos, oficina, música, audio y edición de video, programación. Última Versión 2020
  \item \href{https://escuelaslinux.sourceforge.io/}{Escuelas Linux}: Escuelas Linux es una distribución, diseñada para implementar Software Libre en educación, es bastante popular y moderna, está basada en Bodhi linux ( una distribución liviana a su vez basada en Ubuntu). Última versión 2020
  \item \href{http://edulibre.net/}{EdulibreOs}: EdulibreOs es una versión de linux desarrollada por Edulibre, una organización latinoamericana que busca trabajar la educación de calidad en los niños y niñas basándose en los principios del Código Abierto. Se viene desarrollando desde el 2008 hasta ahora. Van por su versión 8. Basada en Ubuntu con Escritorio Innova. Última versión 2016
  \item \href{https://portal.edu.gva.es/lliurex}{LliureX}: Desarrollado para el sistema educativo de Valencia, España. Está basada en Ubuntu y tiene interfaz gráfica KDE. Tiene un buen ritmo de desarrollo, llevan 19 versiones y se actualiza seguido. Última versión 2019
  \item \href{https://wiki.debian.org/DebianEdu/}{Debian Edu}: Debian Edu / Skolelinux es una distribución de Linux, hecha por el proyecto Debian Edu. Siendo una distribución \href{https://jenkins.debian.net/wiki.debian.org/DebianPureBlends}{mezclada de Debian} es un sub\-proyecto oficial de Debian . Está pensada para armar una red escolar, especialmente en aquellos casos que quieren armar un laboratorio con un servidor y varias computadoras con pocas potencias conectadas a ese servidor (thin clients). \href{https://jenkins.debian.net/userContent/debian-edu-doc/debian-edu-doc-es/debian-edu-bullseye-manual.html}{Manual y +info}. Última versión 2020
\end{itemize}
